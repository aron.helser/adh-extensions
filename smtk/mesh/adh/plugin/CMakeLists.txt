smtk_add_plugin(smtkADHMeshPlugin
  REGISTRAR smtk::mesh::adh::Registrar
  REGISTRAR_HEADER smtk/mesh/adh/Registrar.h
  MANAGERS smtk::operation::Manager
  PARAVIEW_PLUGIN_ARGS
    VERSION "1.0")

target_link_libraries(smtkADHMeshPlugin
  PUBLIC
    smtkADHMesh
    smtkCore)
