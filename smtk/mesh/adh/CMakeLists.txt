set(adhSrcs
  MeshPartition.cxx
  Registrar.cxx
)

set(adhHeaders
  MeshPartition.h
  Registrar.h
)

set(operations
  AnnotateCanonicalIndex
  GenerateHotStartData
)
foreach(operation ${operations})
  smtk_encode_file("${CMAKE_CURRENT_SOURCE_DIR}/operators/${operation}.sbt"
    TYPE "_xml"
    HEADER_OUTPUT operationXMLHeader
  )

  list(APPEND adhHeaders "operators/${operation}.h" "${operationXMLHeader}")
  list(APPEND adhSrcs "operators/${operation}.cxx")
endforeach()

add_library(smtkADHMesh ${adhSrcs} ${adhHeaders})

target_include_directories(smtkADHMesh PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}>
)

set(moab_libs ${MOAB_LIBRARIES})
if(WIN32)
  set(moab_libs "MOAB")
endif()

target_link_libraries(smtkADHMesh
  LINK_PUBLIC
    smtkCore
  LINK_PRIVATE
    ${moab_libs}
)

install(
  TARGETS smtkADHMesh
  EXPORT  ${PROJECT_NAME}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

smtk_export_header(smtkADHMesh Exports.h)

install(
  FILES MeshPartition.h Registrar.h
  DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}/smtk/mesh/adh")

add_subdirectory(pybind11)
