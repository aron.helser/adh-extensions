//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include <pybind11/pybind11.h>
#include <utility>

namespace py = pybind11;

template <typename T, typename... Args>
using PySharedPtrClass = py::class_<T, std::shared_ptr<T>, Args...>;

#include "PybindAnnotateCanonicalIndex.h"
#include "PybindGenerateHotStartData.h"
#include "PybindRegistrar.h"

PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);

PYBIND11_MODULE(_smtkPybindADHMesh, m)
{
  m.doc() = "<description>";
  py::module smtk = m.def_submodule("smtk", "<description>");
  py::module adh = smtk.def_submodule("adh", "<description>");

  PySharedPtrClass< smtk::mesh::adh::AnnotateCanonicalIndex, smtk::operation::XMLOperation > smtk_adh_AnnotateCanonicalIndex = pybind11_init_smtk_adh_AnnotateCanonicalIndex(adh);
  PySharedPtrClass< smtk::mesh::adh::GenerateHotStartData, smtk::operation::XMLOperation > smtk_adh_GenerateHotStartData = pybind11_init_smtk_adh_GenerateHotStartData(adh);

  py::class_< smtk::mesh::adh::Registrar > smtk_adh_Registrar = pybind11_init_smtk_adh_Registrar(adh);
}
