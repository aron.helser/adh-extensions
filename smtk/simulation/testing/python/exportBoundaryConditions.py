#=============================================================================
#
#  Copyright (c) Kitware, Inc.
#  All rights reserved.
#  See LICENSE.txt for details.
#
#  This software is distributed WITHOUT ANY WARRANTY; without even
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#  PURPOSE.  See the above copyright notice for more information.
#
#=============================================================================
import os
import sys
import unittest
import smtk
import smtk.mesh
import smtk.model
import smtk.testing
import smtk.simulation

import smtkmeshadh
import smtksimulationadh

ExportScope = type('ExportScope', (object,), dict())

class ExportBoundaryConditions(smtk.testing.TestCase):

    def testExport(self):
        exportBoundaryConditions = smtk.simulation.adh.ExportBoundaryConditions()

        scope = ExportScope()
        scope.sim_atts = smtk.attribute.Resource.create()
        scope.mesh_collection = smtk.mesh.Resource.create()
        scope.output_filebase = 'output'
#        exportBoundaryConditions(scope)


if __name__ == '__main__':
    smtk.testing.process_arguments()
    unittest.main()
