//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================

#include "smtk/simulation/adh/ExportVegetation.h"

#include "smtk/extension/vtk/io/mesh/ImportVTKData.h"

#include "smtk/io/mesh/MeshIOXMS.h"

#include "smtk/mesh/core/Resource.h"

#include "smtk/model/AuxiliaryGeometry.h"
#include "smtk/model/Instance.h"
#include "smtk/model/Resource.h"

//force to use filesystem version 3
#define BOOST_FILESYSTEM_VERSION 3
#include <boost/filesystem.hpp>

#include <iomanip>
#include <map>

namespace
{

template <typename Type>
Type getPyAttribute(pybind11::object& object, const std::string& name)
{
  if (!pybind11::hasattr(object, name.c_str()))
  {
    std::cerr << "could not find " << name << std::endl;
    return Type();
  }
  return object.attr(name.c_str()).cast<Type>();
}

}

namespace smtk
{
namespace simulation
{
namespace adh
{

void ExportVegetation::operator()(pybind11::object py_scope)
{
  // Access the attribute resource describing the simulation
  auto model_resource = getPyAttribute<smtk::model::ResourcePtr>(py_scope, "model_resource");

  // Compose the geometry and boundary condition ouput file names
  auto output_filebase = getPyAttribute<std::string>(py_scope, "output_filebase");
  auto output_directory = getPyAttribute<std::string>(py_scope, "output_directory");

  boost::filesystem::path metFile =
    boost::filesystem::path(output_directory) / output_filebase;
  metFile += ".met";

  boost::filesystem::path vegetationFile =
    boost::filesystem::path(output_directory) / output_filebase;
  vegetationFile += "_vegetation.2dm";

  return operator()(metFile.string(), vegetationFile.string(), model_resource);
}

void ExportVegetation::operator()(
  const std::string& metFileName,
  const std::string& vegetationName,
  const smtk::model::ResourcePtr& modelResource)
{
  // Access the auxiliary geometry associated with the model resource.
  auto auxiliaryGeometries = modelResource->
    entitiesMatchingFlagsAs<smtk::model::AuxiliaryGeometries>(smtk::model::AUX_GEOM_ENTITY);
  if (auxiliaryGeometries.empty())
  {
    std::cerr << "Could not access auxiliary geometry";
    return;
  }
  // Generation of vegetation mesh disabled.
  // smtk::model::AuxiliaryGeometry auxiliaryGeometry = auxiliaryGeometries[0];

  // smtk::extension::vtk::io::mesh::ImportVTKData importVTKData;
  // smtk::mesh::Resource::Ptr meshResource = smtk::mesh::Resource::create();
  // if (!importVTKData(auxiliaryGeometry.url(), meshResource, ""))
  // {
  //   std::cerr << "Could not import auxiliary geometry into smtk::mesh";
  //   return;
  // }

  // smtk::io::mesh::MeshIOXMS exportMesh;
  // if (!exportMesh.exportMesh(vegetationName, meshResource))
  // {
  //   std::cerr << "Could not export mesh into XMS format";
  //   return;
  // }

  {
    boost::filesystem::path vegetationInpFile =
      boost::filesystem::path(vegetationName).replace_extension(".dat");

    std::ofstream out;
    out.open(vegetationInpFile.string().c_str());

    std::map<std::string, int> fileIndices;

    out << "MODELS " << auxiliaryGeometries.size() << "\n";
    for (std::size_t i = 0; i < auxiliaryGeometries.size(); ++i)
    {
      out << "mod" << std::setfill('0') << std::setw(2) << i << " "
          << auxiliaryGeometries[i].url() << "\n";
          fileIndices[auxiliaryGeometries[i].url()] = i;
    }
    out << "\n";

    out << "LEAFSIZE "<<auxiliaryGeometries.size() << "\n";
    for (std::size_t i = 0; i < auxiliaryGeometries.size(); ++i)
    {
      out << "mod" << std::setfill('0') << std::setw(2) << i << " "
          << (auxiliaryGeometries[i].hasFloatProperty("leafsize") ?
              auxiliaryGeometries[i].floatProperty("leafsize")[0] : 0.01) << "\n";
    }
    out << "\n";

    out << "MATERIALCODE 1\n";
    for (std::size_t i = 0; i < auxiliaryGeometries.size(); ++i)
    {
      out << "mod" << std::setfill('0') << std::setw(2) << i << " 200\n";
    }
    out << "NODEFILE " << "veg/veg_Temp" << "\n";
    out << "START_SIM_TIME " << 247 << "\n";
    out << "END_SIM_TIME " << 249 << "\n";
    out << "MET_WIND_HEIGHT " << 3 << "\n";
    // Still need the keyword, indicate no file is ouput
    out << "OUTPUT_MESH " << "disabled" << "\n";
    out << "MET_FILE " << metFileName << "\n";
    out << "INPUT_FLUX_FILE " << 500 << "\n";
    out << "\n";

    // Access the placements associated with the model resource.
    auto instances = modelResource->
      entitiesMatchingFlagsAs<smtk::model::Instances>(smtk::model::INSTANCE_ENTITY);

    std::size_t nInstances = 0;
    for (auto& instance : instances)
    {
      nInstances += instance.numberOfPlacements();
    }

    out << "INSTANCE " << nInstances << "\n";
    for (std::size_t i = 0; i < instances.size(); ++i)
    {
      const smtk::model::Tessellation* tessellation = instances[i].hasTessellation();
      if (!tessellation)
      {
        continue;
      }
      for (std::size_t j = 0; j < tessellation->coords().size(); j+=3)
      {
        double xyz[3];
        for (std::size_t k = 0; k < 3; ++k)
        {
          xyz[k] = tessellation->coords()[j + k];
        }

        // Multiple aux geometry handled via passed-in filename.
        std::size_t auxIndex = 0;
        if (instances[i].hasStringProperty("fileUrl"))
        {
          auxIndex = fileIndices[instances[i].stringProperty("fileUrl")[0]];
        }
        out << "mod" << std::setfill('0') << std::setw(2) << auxIndex << " "
            << (instances[i].hasFloatProperty("scale") ? instances[i].floatProperty("scale")[0] : 1)
            << " "
            << (instances[i].hasFloatProperty("rotate") ?
                instances[i].floatProperty("rotate")[2] : 0.)
            << " ";
        if (instances[i].hasFloatProperty("translate"))
        {
          for (std::size_t k = 0; k < 3; ++j)
          {
            out << xyz[k] + instances[i].floatProperty("translate")[k];
            if (k != 2)
            {
              out << " ";
            }
          }
        }
        else
        {
          out << xyz[0] << " " << xyz[1] << " " << xyz[2];
        }
        out << "\n";
      }
    }
  }
}

}
}
}
