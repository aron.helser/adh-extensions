set(module_files
  "${CMAKE_CURRENT_SOURCE_DIR}/meshing/vtk.module"
  "${CMAKE_CURRENT_SOURCE_DIR}/reader/vtk.module"
)
vtk_module_scan(
  MODULE_FILES ${module_files}
  PROVIDES_MODULES vtk_modules
  HIDE_MODULES_FROM_CACHE ON
  WANT_BY_DEFAULT ON)
vtk_module_build(
  MODULES ${vtk_modules}
  PACKAGE SMTKADHVTKModules
  INSTALL_EXPORT SMTKADHVTKModules
  HEADERS_DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${PROJECT_NAME}/${PROJECT_VERSION}"
  TEST_DIRECTORY_NAME "NONE")

set_property(GLOBAL
  PROPERTY
    _smtk_vtk_extention_modules "${vtk_modules}")
